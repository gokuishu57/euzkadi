import scrapy
import pycountry
from locations.items import GeojsonPointItem
from locations.categories import Code
from typing import List, Dict


class Euzkadi(scrapy.Spider):
    name = 'euzkadi_mex_dac'
    brand_name = 'Euzkadi'
    spider_type: str = 'chain'
    spider_categories: List[str] = [Code.CAR_REPAIR_SERVICE]
    allowed_domains: List[str] = ['euzkadi.mx/']
    start_urls = ['https://dealerlocator.continental-tyres.com/api/v1/dealers/within_bounds.json?bbox%5Bsw_lat%5D=10.7386305038768&bbox%5Bsw_lon%5D=-111.83828025625&bbox%5Bne_lat%5D=34.80683462891845&bbox%5Bne_lon%5D=-93.51308494375&brand_key=euzkadi&only_featured=false&max_detailed=8000&max_all_details=400&market_key=mx&business_unit_key=plt&attributes=']

    def start_requests(self):
        headers = {            
            "Authorization": "Token On9UaUJGfifbr7iv8Wip3Qtt",        
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"
        }


        for url in self.start_urls:
                yield scrapy.Request(url=url, headers=headers, callback=self.parse)



    def parse(self, response):
        if response.status == 200:
            # Parse the response here
            print("[Scraping Begins...]")
            responseData = response.json()

            for row in responseData:
                id = row['id']
                name = row['name']
                street = row['address'].get('street', '')
                city = row['address'].get('city', '')
                state = row['address'].get('state', '')
                country = row['address'].get('country', '')
                pincode = row['address'].get('zip_code', '')
                addr_components = [name, street, city, state, country, pincode]
                addr_full = ", ".join(component for component in addr_components if component is not None)
                website = 'https://www.euzkadi.mx/'
                store_url = row['url']
                email = row['email']
                phone = row['phone']
                lat = float(row['location']['lat'])
                lon = float(row['location']['lng'])


                item = GeojsonPointItem()
                item['ref'] = id
                item['name'] = name
                item['addr_full'] = addr_full
                item['street'] = street
                item['city'] = city
                item['state'] = state
                item['country'] = country
                item['postcode'] = pincode
                item['website'] = website
                item['phone'] = phone
                item['store_url'] = store_url
                item['email'] = email
                item['chain_id'] = '5133'
                item['chain_name'] = 'Euzkadi 78 MEX'
                item['categories'] = 'Car_REPAIR_SERVICE'
                item['brand'] = self.brand_name
                item['lat'] = lat
                item['lon'] = lon

                yield item
        else:
            # Handle the error case
            print(f"Error: {response.status} - {response.text}")